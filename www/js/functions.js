// code voor de formulier validatie en het verzenden

$(document).ready(function(){
	
	// scripts voor het formulier
			
			//global vars  
			var searchBoxes = $(".veldje");  
			var searchBox1 = $("#naam");
			var searchBox2 = $("#email");
			var searchBox2Default = document.getElementById('email').defaultValue;
			var searchBox1Default = document.getElementById('naam').defaultValue;
			
			$('.button').click(function() {
				if ( popupStatus == 1 ) {
					searchBox1.focus();
				}
			});
			
			// voeg de class toe als de focus op het veld wordt gelegd  
			searchBoxes.focus(function(e){  
    			$(this).addClass("active");  
			});  
			
			searchBoxes.blur(function(e){  
   				$(this).removeClass("active");  
			});  
			
			searchBox2.focus(function(){  
    			if($(this).attr("value") == searchBox2Default) $(this).attr("value", "");  
			});  
			
			searchBox2.blur(function(){  
    			if($(this).attr("value") == "") $(this).attr("value", searchBox2Default);  
			});
			
			searchBox1.focus(function(){  
    			if($(this).attr("value") == searchBox1Default) $(this).attr("value", "");  
			});  
			
			searchBox1.blur(function(){  
    			if($(this).attr("value") == "") $(this).attr("value", searchBox1Default);  
			});
	
	
	$('.error').hide();
	$("#knopje").click(function() {  
    	// validate and process form here  
    	$('.error').hide();  
      	var name = $("input#naam").val();  
        if (name == "" || name == searchBox1Default) {  
      		$("span#naam_error").show();  
      		$("input#naam").focus();  
      		return false;  
    	}
    	
        var email = $("input#email").val();  
        if (email == "" || email == searchBox2Default) {  
      		$("span#email_error").show();  
      		$("input#email").focus();  
      		return false;  
    	}  
    
    	var dataString = 'name='+ name + '&email=' + email;  
		//alert (dataString);return false;  
		$.ajax({  
  			type: "POST",  
  			url: "about.htm",  // hier een link naar je php script...
  			data: dataString,  
  			success: function() {  
    			$('#contactArea').html("<div id='message'></div>");  
    			$('#message').html("<h2>Contact Form Submitted!</h2>")  
    			.append("<p>We will be in touch soon.</p>")  
    			.hide()  
    			.fadeIn(1500);
  			}  
  			
		});  
		
		
		
		return false;  
    
  	});
  	
});