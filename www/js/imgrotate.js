    	$("document").ready( function(){
    		
    		setInterval( "imgRotate()", 2000 );
    		
    	});
    	
    	function imgRotate(){
    		
    		var curFoto = $( "#photoShow div.current" );
    		var nextFoto = curFoto.next();
    		
    		if( nextFoto.length == 0 ){
    			nextFoto = $( "#photoShow div:first" );
    		}
    		
    		curFoto.removeClass( "current" ).addClass("previous");
    		nextFoto.css( "opacity", 0.0 ).addClass( "current" ).animate( { opacity:1.0 }, 1500, function(){
    		
    			curFoto.removeClass( "previous" );
    		
    		});
    		
    		
    	};