function validateForm(){
	var correct= true,
		error= "",
		company=document.getElementById("company").value,
		companyError=document.getElementById("companyError").style,
		defaultCompany=document.getElementById("company").defaultValue,
		jobTitle=document.getElementById("jobTitle").value,
		jobError=document.getElementById("jobError").style,
		defaultJob=document.getElementById("jobTitle").defaultValue,
		lastName=document.getElementById("lastName").value,
		lastError=document.getElementById("lastError").style,
		defaultLast=document.getElementById("lastName").defaultValue,
		firstName=document.getElementById("firstName").value,
		firstError=document.getElementById("firstError").style,
		defaultFirst=document.getElementById("firstName").defaultValue,
		tel=document.getElementById("tel").value,
		telError=document.getElementById("telError").style,
		defaultTel=document.getElementById("tel").defaultValue,
		email=document.getElementById("email").value,
		emailError=document.getElementById("emailError").style,
		defaultEmail=document.getElementById("email").defaultValue,
		res = valideerEmail( email ),
		selectCountry=document.getElementById("country"),
		countryError=document.getElementById("countryError").style,
		selectSubject=document.getElementById("subject"),
		subjectError=document.getElementById("subjectError").style;
		
		
		if( company.length < 2 || company == defaultCompany ){
				
				error += "\nPlease provide us your company name";
				
				correct = false;
				
				companyError.display = "inline";
			} else {
				
				companyError.display = "none";				
			}
			
		if( jobTitle.length < 2 || jobTitle == defaultJob ){
				
				error += "\nYou forgot to mention your job title";
				
				correct = false;
				
				jobError.display = "inline";
			} else {
				
				jobError.display = "none";				
			}
			
		if( lastName.length < 2 || lastName == defaultLast ){
				
				error += "\n Your Last Name is required";
				
				correct = false;
				
				lastError.display = "inline";
			} else {
				
				lastError.display = "none";				
			}
			
		if( firstName.length < 2 || firstName == defaultFirst ){
				
				error += "\n Your First Name is required";
				
				correct = false;
				
				firstError.display = "inline";
			} else {
				
				firstError.display = "none";				
			}
			
		if(selectCountry.selectedIndex == ""){
			error += "\n Select your Country";
			correct = false;
			countryError.display = "inline";
		} else{
			countryError.display = "none";
		}
		
		if( tel.length != 10 || isNaN(tel) || tel == defaultTel ){
				
				error += "\nFill in your 10 digits telephone number";
				
				correct = false;
				
				telError.display = "inline";
				
			} else {
				
				telError.display = "none";
				
			}
			
					
			if( !res || email == defaultEmail ){
				
				error += "\n The email address is not valid";
				
				correct = false;
				
				emailError.display = "inline";
				
			} else {
   				
   				emailError.display = "none";
   				
			}
			
			if(selectSubject.selectedIndex == "0"){
			error += "\n Select the Subject";
			correct = false;
			subjectError.display = "inline";
		} else{
			subjectError.display = "none";
		}
			
			
		
		if (correct){
			return true;
			
		}else{
			alert("There are few mistakes that you have to fix" + error);
			return false;
		}
		
}// end validateForm()

		function resetForm(){
		
	var companyError=document.getElementById("companyError").style,			
		jobError=document.getElementById("jobError").style,		
		lastError=document.getElementById("lastError").style,		
		firstError=document.getElementById("firstError").style,		
		telError=document.getElementById("telError").style,		
		emailError=document.getElementById("emailError").style,
		countryError=document.getElementById("countryError").style,
		subjectError=document.getElementById("subjectError").style;
			
			companyError.display = "none";
			jobError.display = "none";
			lastError.display = "none";
			firstError.display = "none";
			telError.display = "none";
			countryError.display = "none";
			subjectError.display = "none";
			emailError.display = "none";
			
		} // einde resetForm()

		function valideerEmail( patroon ){
			
			var emailpatroon = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
			
			return emailpatroon.test( patroon );
			
		}
		
		function resetText(field){
			if (field.value == ""){
				field.value = field.defaultValue;
				field.style.color = "#a4a3a3";
			}
			
		}
		
		function clearText(field){
			if (field.value == field.defaultValue){
				field.value = "";
				field.style.color = "#000";
			}
			
		}
		

		function checkJavascript(){
			
			document.getElementById("contactForm").style.display = "block";
			document.getElementById("noJS").style.display = "none";
			
		}